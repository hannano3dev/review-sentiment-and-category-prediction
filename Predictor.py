import json
from fastapi import FastAPI, HTTPException 
from pydantic import BaseModel
from transformers import AutoModelForSequenceClassification, AutoTokenizer, pipeline

SENTIMENT_CATEGORY_JSON = "Sentiment and Category.json" 
MODEL_PATH = "Model Files\deberta-v3-xsmall-zeroshot-v1.1-all-33" 

app = FastAPI()

with open(SENTIMENT_CATEGORY_JSON, 'r') as file:
    json_data = json.load(file)
SENTIMENT_SEQUENCE = json_data["sentiment_list"]
CATEGORY_SEQUENCE = json_data["category_list"]

# Load Model and Tokenizer
model = AutoModelForSequenceClassification.from_pretrained(MODEL_PATH)
tokenizer = AutoTokenizer.from_pretrained(MODEL_PATH)
classifier = pipeline("zero-shot-classification", model=model, tokenizer=tokenizer)

class Item(BaseModel):
    review: str 

def sentiment_inference(review, sentiments):
    sentimentAnalysis = classifier(review, sentiments)
    predictedSentiment = sentimentAnalysis['labels'][0]
    predictedScore = sentimentAnalysis['scores'][0]
    return predictedSentiment, predictedScore

def category_inference(review,categories):
    categoryAnalysis = classifier(review, categories)
    predictedCategory = categoryAnalysis['labels'][0]
    return predictedCategory

@app.post("/api/process_review")
async def process_review(item: Item):
    try:

        sentiment, sentimentScore = sentiment_inference(item.review, SENTIMENT_SEQUENCE)
    
        category = category_inference(item.review, CATEGORY_SEQUENCE)
        
        return {"Text": item.review, "Sentiment": sentiment, "Sentiment Score": sentimentScore, "Category":category}

    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Error prcoessing the review: {str(e)}")

