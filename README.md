## Packages Required

 1. FastAPI
 2. Transformers
 3. Torch
 4. Uvicorn if you want to test API on local host
 5. Python version being used 3.10.11   

## What this code does ?
This API is created to run inference on the model for sentiment prediction and category prediction. The input for this model is review of any mobile application from Play Store and App Store. The model predicts the sentiment and category of the given review.
 
## How to run this code ?
After installing the required packages you can run `uvicorn Predictor:app --reload`in the terminal. This will run model on your localhost. The API can be tested by visiting http://127.0.0.1:8000/docs after running the script using uvicorn.

## Inference Time
**Without GPU**

 - Sentiment only: 0.46 sec
 - Category only: 1.6 sec
 - Sentiment + Category: 2.0 sec




